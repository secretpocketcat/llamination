﻿namespace SPC.GameDev.Testing
{
    using UnityEngine;

    public class Llamination : MonoBehaviour
	{
        private readonly string greyscaleMultPropName = "_GreyscaleMultiplier";
        private readonly string tintPropName = "_Color";

        public Material Material;
        public Gradient TintGradient;

        private void Start()
        {
            this.Animate(0, 0);
        }

        private void OnDestroy()
        {
            this.Animate(0, 0);
        }

        private void Update()
        {
            var greyscaleMult = Mathf.Sin(Time.time * 3f) / 2 + .5f;
            var tintMult = Mathf.Sin(Time.time * .95f) / 2 + .5f;
            this.Animate(greyscaleMult, tintMult);
        }

        private void Animate(float greyscaleMult, float tintMult)
        {
            this.Material.SetFloat(this.greyscaleMultPropName, greyscaleMult);

            var tint = this.TintGradient.Evaluate(tintMult);
            var finalTint = Color.Lerp(tint, Color.white, greyscaleMult);
            this.Material.SetColor(this.tintPropName, finalTint);
        }
    }
}
